<?php
  session_start(); // Pour les massages

  // Contenu du formulaire :
  $nom =  htmlentities($_POST['nom']);
  $prenom = htmlentities($_POST['prenom']);
  $email =  htmlentities($_POST['email']);
  $password = htmlentities($_POST['password']);
  
  $role = 1; // 1 pour membre, 2 pour Admin

  // Option pour bcrypt
  $options = [
        'cost' => 12,
  ];
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);  
  // Connexion :
  
require_once("param.inc.php");
$mysqli = new mysqli($host, $login, $passwd, $dbname);
if ($mysqli->connect_error) {
    die('Erreur de connexion (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

  // Attention, ici on ne vérifie pas si l'utilisateur existe déjà
  try{
    $stmt = $mysqli->prepare("INSERT INTO User_U(Nom_user_U, Prenom_U, 
    email, mot_de_passe, type_user)   VALUES ( ?, ?, ?, ?, ?)");
    $password = password_hash($password, PASSWORD_BCRYPT, $options);
    echo "\t\n hashed password before adding to the database" .$password."";
    $stmt->bind_param("ssssi", $nom, $prenom, $email, $password,$role);
    // Le message est mis dans la session, il est préférable de séparer message normal et message d'erreur.
    if($stmt->execute())
      $_SESSION['message'] = "Enregistrement réussi";
      $_SESSION["success"] = true;
      require_once("db_connexion.php")  ;
      goto_page("accueil2.php"); //Redirection vers la page d'accueil 
  }
  catch(mysqli_sql_exception $e) {
    if(str_ends_with($e->getMessage(),"'user_u.UN_email'"))
    $_SESSION["message"] ="Cet email existe déjà";
    else{
        $_SESSION["message"] = $e->getMessage();
    }
    goto_page("erreur_inscription.php");
  }
  // Redirection vers la page d'accueil :
  header('Location: accueil2.php');

?>