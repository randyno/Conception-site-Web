<?php
  session_start(); // Pour les massages

  // Contenu du formulaire :
  $nomJ =  htmlentities($_POST['jeu']);
  $dateJ = strtotime($_POST['date']);
  $heureJ =  strval($_POST['time']);
  $duree = strval($_POST['duree']);
  
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);  
  // Connexion :
require_once("param.inc.php");
$mysqli = new mysqli($host, $login, $passwd, $dbname);
if ($mysqli->connect_error) {
    die('Erreur de connexion (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

  // Attention, ici on ne vérifie pas si l'utilisateur existe déjà
  try{
    $stmt_partie = $mysqli->prepare("INSERT INTO 
    Partie_P(jeu_id_P,      date_P,             heure,           duree) 
    VALUES ( 2,to_date('y-m-d', ?) , to_date('hh:mm', ?), to_date('y-m-d', ?))");
    $stmt_ajoue= $mysqli->prepare("INSERT INTO 
    A_joue_A(id_user_A,id_partie_A)
    VALUES(?,   )");
    $password = password_hash($password, PASSWORD_BCRYPT, $options);
    echo "\t\n hashed password before adding to the database" .$password."";
    $stmt->bind_param("ssssi", $nom, $prenom, $email, $password,$role);
    // Le message est mis dans la session, il est préférable de séparer message normal et message d'erreur.
    if($stmt->execute())
        $_SESSION['message'] = "Enregistrement réussi";
        echo "\t\n hashed password after adding to the database" .$password."";
  
  }
  catch(mysqli_sql_exception $e) {
    if(str_ends_with($e->getMessage(),"'user_u.UN_email'"))
      echo "Cet email existe déjà";
    else{
        $_SESSION["message"] = $e->getMessage();
    }
  }
  // Redirection vers la page d'accueil :
  header('Location: accueil2.php');

?>