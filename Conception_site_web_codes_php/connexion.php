<?php
session_start();

$email =  htmlentities($_POST['email']);
$password = htmlentities($_POST['password']);

// Option pour bcrypt
$options = [
  'cost' => 12,
];
// Connexion à la base de données:
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);  
require_once("param.inc.php");
echo "Tentative de connexion a la base de données.";
$mysqli = new mysqli($host, $login, $passwd, $dbname);
if ($mysqli->connect_error) {
    die('Erreur de connexion (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
} 
echo "  Connexion à la base de données réussie";
 // Vérification du mot de passe entré par l'utilisateur
try{
  $_SESSION["message"]= "RAS";
  $prepared_statement = $mysqli->prepare("select id_user_u, type_user as type, mot_de_passe,prenom_u
  from User_U where email like ?");
  $prepared_statement->bind_param("s", $email); 
  $prepared_statement->execute();
  $result_set = $prepared_statement->get_result();
  if($result_set->num_rows > 0){
    $row = $result_set->fetch_assoc();
    $DB_hashed = $row["mot_de_passe"];
    $ch = password_verify($password, $DB_hashed); 
    echo " password verify ".$ch ;
    if( password_verify($password, $DB_hashed)==1 ){
      $_SESSION["message"] = "Authentification reussie pour un rôle inconnu" ;
      $_SESSION["connected?"]=true;
      $_SESSION["id_user"] = $row["id_user_u"];
      if( $row["type"] == 1){
        $_SESSION["message"] = "Authentification reussie en tant que membre" ;
        echo "<meta http-equiv='refresh' content='0;url=membre/page_membre.php'>";
        exit();
      }if( $row["type"] == 2){
        $_SESSION["message"] = "Authentification reussie en tant qu'administrateur)" ;
        echo "<meta http-equiv='refresh' content='0;url=admin/page_admin.php'>";
        exit();
      }
    }
    else{
      $_SESSION["message"] = "Echec de l'authentification. Revérifiez votre mot de passe";
      require_once("db_connexion.php");
      goto_page("erreur_connexion.php");
      //header('Location: page.php');
    }
  }
  else{
    $_SESSION["message"] ="Cette email n'existe pas dans notre base de données!";
    require_once("db_connexion.php");
    goto_page("erreur_connexion.php");
  }
}
catch(mysqli_sql_exception $e){
  $_SESSION["message"] = $e->getMessage();
  require_once("db_connexion.php");
  goto_page("erreur_connexion.php");
}

/*
    // Le message est mis dans la session, il est préférable de séparer message normal et message d'erreur.
    if($stmt->execute()) {
        $_SESSION['message'] = "Enregistrement réussi";

    } else {
        $_SESSION['message'] =  "Impossible d'enregistrer";
    }*/
    /*$titre = "Connexion";
    include 'header.inc.php';
    include 'menu.inc.php';*/
?>
