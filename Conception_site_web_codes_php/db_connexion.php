<?php
//session_start();
function goto_page($path){
    echo "<meta http-equiv='refresh' content='0;url=$path>";
}
function fetch_user_name(int $user_id){
    require "param.inc.php";
    $mysqli = new mysqli($host, $login, $passwd, $dbname);
    if ($mysqli->connect_error) {
      die('Erreur de connexion (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
    } 
    try{
        $prepared_statement = $mysqli->prepare("select prenom_u as prenom, nom_user_u as nom
        from User_U where id_user_u=?");
        $prepared_statement->bind_param("i", $user_id); 
        $prepared_statement->execute();
        $result_set = $prepared_statement->get_result();
        if($result_set->num_rows > 0){
          $row = $result_set->fetch_assoc();
            $_SESSION["prenom"] = $row["prenom"];
            $_SESSION["nom"] = $row["nom"];
            $_SESSION["message"] = "Authentification reussie pour un rôle inconnu" ;
            return 1;
        }else{
            $_SESSION["message"] = 0;
            return 0;
        } 
      }
      catch(mysqli_sql_exception $e){
        echo "". $e->getMessage();
        return 0;
      }
}

?>